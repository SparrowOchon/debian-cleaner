# Debian Cleaner
Remove package residue and orphaned packages left by program installs on debian.

## Usage
Running the cleanup script will automatically install the deborphan and debfoster packages, if debfoster is already installed and configed the premade config will be run.

## Thanks
Thanks to the developers of Debfoster and Deborphan for their amazing tools.

